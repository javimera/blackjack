//This program is derived from the Microsoft DLL cards.dll.  It used the images from
//that DLL.  It allows any of the cards in deck to be drawn at a speicified (x,y) positon
//within the current device context.
//This code was re-written by W.G. King 
//Copyright 2009
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace CardsClass
{
    public class Card
    {
        #region constants
        /// <summary>
        /// Special cards
        /// </summary>
        public const int mdFaceUp  = 0;  //Draw card face up, card to draw specified by cd 
        public const int mdFaceDown = 1; //Draw card face down, back specified by cd cdFaceDownFirst..cdFaceDownLast) 
        public const int mdHilite  =  2; //Same as FaceUp except drawn with NOTSRCCOPY mode 
        public const int mdGhost   =  3; //Draw a ghost card -- for ace piles
        public const int mdRemove  =  4; //draw background specified by rgbBgnd
        public const int mdInvisibleGhost =  5; //?
        public const int mdDeckX  =   6; //Draw X card (end of deck marker)
        public const int mdDeckO  =   7; //Draw O card (end of deck marker)
        
        /// <summary>
        /// Suit and card indices.  Orders of BOTH are important
        /// </summary>
       
        public const int suClub   =           0;
        public const int suDiamond =          1;
        public const int suHeart   =          2;
        public const int suSpade   =          3;
        public const int suMax     =          4;
        public const int suFirst   =          suClub;

        /// <summary>
        /// Face cards indices
        /// </summary>
        /// 
        public const int raAce     =          0;
        public const int raTwo     =          1;
        public const int raThree   =          2;
        public const int raFour    =          3;
        public const int raFive    =          4;
        public const int raSix     =          5;
        public const int raSeven   =          6;
        public const int raEight   =          7;
        public const int raNine    =          8;
        public const int raTen     =          9;
        public const int raJack    =          10;
        public const int raQueen   =          11;
        public const int raKing    =          12;
        public const int raMax     =          13;
        public const int raNil     =          15;
        public const int raFirst   =          raAce;

       /// <summary>
       /// Face down cards
       /// </summary>
       /// 
        public const int cdFaceDown1 = 54;
        public const int cdFaceDown2 = 55;
        public const int cdFaceDown3 = 56;
        public const int cdFaceDown4 = 57;
        public const int cdFaceDown5 = 58;
        public const int cdFaceDown6 = 59;
        public const int cdFaceDown7 = 60;
        public const int cdFaceDown8 = 61;
        public const int cdFaceDown9 = 62;
        public const int cdFaceDown10 = 63;
        public const int cdFaceDown11 = 64;
        public const int cdFaceDown12 = 65;
        public const int cdFaceDownFirst = cdFaceDown1;
        public const int cdFaceDownLast = cdFaceDown12;

        #endregion

        #region enumerations
        //Enumeration representing above Mode Constants

        public enum modes
        {
            Hilite = mdHilite, Ghost = mdGhost,
            Remove = mdRemove, Invisible = mdInvisibleGhost, DeckX = mdDeckX,
            DeckO = mdDeckO
        };
        public enum Face { Up = mdFaceUp, Down = mdFaceDown };
        public enum Ranks{Ace = raAce, Two = raTwo, Three = raThree, Four = raFour, Five = raFive,
                         Six = raSix, Seven = raSeven, Eight = raEight, Nine = raNine, Ten = raTen,
                         Jack = raJack, Queen = raQueen, King = raKing, Max = raMax, Nil = raNil, First = raFirst};

        //Enumeration representing above Suit Constants
        public enum Suits { Club = suClub, Diamond = suDiamond, Heart = suHeart, Spade = suSpade, First = suFirst };     
        
        public enum Backs { back1 = 54, back2, back3, back4, back5, back6, back7, back8, back9, back10, back11, back12 };
        
        #endregion

        #region Constructors

       /// <summary>
       /// Default constructor
       /// </summary>
       /// 
        public Card()
        {
            Rank = raAce;
            Suit = suClub;
            Back = cdFaceDown1;
        }
      /// <summary>
        /// Non-default Constructor: Initializes card to specific rank and suit
      /// </summary>
      /// <param name="ra"></param>
      /// <param name="su"></param>
        public Card(int ra, int su)
        {
            Rank = ra;
            Suit = su;
            Back = cdFaceDown1;
        }
        /// <summary>
        /// Non-default Constructor: Initializes card to specific rand, suit, and back
        /// </summary>
        /// <param name="ra"></param>
        /// <param name="su"></param>
        /// <param name="bk"></param>
        public Card(int ra, int su, int bk)
        {
            Rank = ra;
            Suit = su;
            Back = bk;
        }
        public Card(modes md)
        {
            Mode = md;
           
        }
        public Card(Card cd)
        {
            this.Rank = cd.Rank;
            this.Suit = cd.Suit;
            this.Back = cd.Back;
        }
        #endregion

        #region Methods and Properties

        /// <summary>
        /// Draws the front or back of card
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="md"></param>
        /// <param name="e"></param>
        public void Draw(int x, int y,Face md, PaintEventArgs e)
        {         
            int card;
            
            string cardstr;
            card = Cd(Rank, Suit);
            if (md == Face.Up)
                cardstr = "_" + card.ToString();
            else
                cardstr = "_" + Back.ToString();
            Image image = (Image) CardsClass.Properties.Resources.ResourceManager.GetObject(cardstr);
            e.Graphics.DrawImage(image, x, y);
        }
        public void Draw(int x, int y, modes md, PaintEventArgs e)
        {
            int card;

            string cardstr;
            if (md == Card.modes.DeckO)
                card = 68;
            else if (md == Card.modes.DeckX)
                card = 67;
            else
                card = Cd(Rank, Suit);
           cardstr = "_" + card.ToString();
           Image image = (Image)CardsClass.Properties.Resources.ResourceManager.GetObject(cardstr);
            e.Graphics.DrawImage(image, x, y);
        }
      
        /// <summary>
        /// public Properties Back, Suit, Rank
        /// </summary>
        public int Back {get ; set ;}

        public int Suit { get; set; }

        public int Rank { get; set; }
        public modes Mode{get;set;}
        
       
        /// <summary>
        /// return card from rank and suit
        /// </summary>
        /// <param name="ra"></param>
        /// <param name="su"></param>
        /// <returns></returns>
        public int Cd(int ra, int su)
        {
            return 13 * su + ra + 1;

        }
        #endregion

    }
 }
