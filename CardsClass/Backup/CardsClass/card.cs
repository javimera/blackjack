using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Runtime.InteropServices;


namespace CardsClass
{
    public class Card
    {
        //properites of a card
        private int rank, suit;
        private int back;  

        

        //External function fo the cards.dll that must be imported into class
        //These functions allow access to the Windows cards.
        //
        //    cdtDraw
        //
        //        Draw a card
        //
        //    Arguments:
        //        IntPtr hdc - handle to device context
        //        Int32 x: upper left corner of the card
        //        Int32 y: upper left corner of the card
        //        Int32 cd: card to draw (depends on md)
        //        Int32 md: mode
        //           mdFaceUp:    draw face up card (cd in cdAClubs..cdKSpades)
        //           mdFaceDown:  draw face down card (cd in cdFaceDown1..cdFaceDown12)
        //           mdHilite:    draw face up card inversely
        //           mdGhost:     draw a ghost card, cd ignored
        //           mdRemove:    draw rectangle of background color at x,y
        //           mdDeckX:     draw an X
        //           mdDeckO:     draw an O
        //        UInt32 rgbBgnd: table background color (only required for mdGhost and mdRemove)
        //
        //    Returns:
        //        TRUE if successful
       
        [DllImport("cards.dll")]
        private static extern bool cdtDraw(IntPtr hdc, Int32 x, Int32 y, Int32 cd, 
                                            Int32 md, UInt32 rgbBgnd);

        //    cdtInit
        //
        //        Initialize cards.dll -- called once at app boot time.
        //        cdtInit must be invoked as shown below
        //           unsafe
        //            {
        //                cdtInit(&dxcard, &dxcard);
        //            }     
        //
        //    Arguments:
        //        int FAR *pdxCard: returns card width
        //        int FAR *pdyCard: returns card height
        //
        //    Returns:
        //        TRUE if successful.
        //
        [DllImport("cards.dll")]
        //private unsafe 
        private static extern bool cdtInit(out int pdxCard, out int pdyCard);

        //Same as cdtDraw except that cards may be sized by dx and dy
        [DllImport("cards.dll")]
        private static extern bool cdtDrawExt(IntPtr hdc, Int32 x, Int32 y, Int32 dx, 
                                            Int32 dy, Int32 cd, Int32 md, UInt32 rgbBgnd);

        //Draws animation on cards that support animation
        [DllImport("cards.dll")]
        private static extern bool cdtAnimate(IntPtr hdc, Int32 cd, Int32 x, Int32 y, Int32 ispr);

        //Call this function with application terminates
        [DllImport("cards.dll")]
        private static extern void cdtTerm();
        
        
        // cdtDraw and cdtDrawExt mode flags

        public const int mdFaceUp  = 0;  //Draw card face up, card to draw specified by cd 
        public const int mdFaceDown = 1; //Draw card face down, back specified by cd cdFaceDownFirst..cdFaceDownLast) 
        public const int mdHilite  =  2; //Same as FaceUp except drawn with NOTSRCCOPY mode 
        public const int mdGhost   =  3; //Draw a ghost card -- for ace piles
        public const int mdRemove  =  4; //draw background specified by rgbBgnd
        public const int mdInvisibleGhost =  5; //?
        public const int mdDeckX  =   6; //Draw X card (end of deck marker)
        public const int mdDeckO  =   7; //Draw O card (end of deck marker)

        //Enumeration representing above Mode Constants

        public enum modes{Up = mdFaceUp,Down = mdFaceDown, Hilite = mdHilite,Ghost = mdGhost, 
                         Remove = mdRemove, Invisible = mdInvisibleGhost, DeckX = mdDeckX,
                         DeckO = mdDeckO};
        //Suit and card indices.  Orders of BOTH are important
       
        public const int suClub   =           0;
        public const int suDiamond =          1;
        public const int suHeart   =          2;
        public const int suSpade   =          3;
        public const int suMax     =          4;
        public const int suFirst   =          suClub;

        //Enumeration representing above Suit Constants
        public enum Suits{Club = suClub, Diamond = suDiamond, Heart = suHeart, Spade = suSpade,First = suFirst};

        public const int raAce     =          0;
        public const int raTwo     =          1;
        public const int raThree   =          2;
        public const int raFour    =          3;
        public const int raFive    =          4;
        public const int raSix     =          5;
        public const int raSeven   =          6;
        public const int raEight   =          7;
        public const int raNine    =          8;
        public const int raTen     =          9;
        public const int raJack    =          10;
        public const int raQueen   =          11;
        public const int raKing    =          12;
        public const int raMax     =          13;
        public const int raNil     =          15;
        public const int raFirst   =          raAce;
        public enum Ranks{Ace = raAce, Two = raTwo, Three = raThree, Four = raFour, Five = raFive,
                         Six = raSix, Seven = raSeven, Eight = raEight, Nine = raNine, Ten = raTen,
                         Jack = raJack, Queen = raQueen, King = raKing, Max = raMax, Nil = raNil, First = raFirst};

        /*-----------------------------------------------------------------------------
        | Face down cds
        -----------------------------------------------------------------------------*/
        public const int cdFaceDown1  =   54;
        public const int cdFaceDown2  =   55;
        public const int cdFaceDown3  =   56;
        public const int cdFaceDown4  =   57;
        public const int cdFaceDown5  =   58;
        public const int cdFaceDown6  =   59;
        public const int cdFaceDown7  =   60;
        public const int cdFaceDown8  =   61;
        public const int cdFaceDown9  =   62;
        public const int cdFaceDown10 =   63;
        public const int cdFaceDown11 =   64;
        public const int cdFaceDown12 =   65;
        public const int cdFaceDownFirst = cdFaceDown1;
        public const int cdFaceDownLast = cdFaceDown12;
        
        public enum Backs { back1 = 54, back2, back3, back4, back5, back6, back7, back8, back9, back10, back11, back12 };
        
        //Default Constructor
        public Card()
        {
            rank = raAce;
            suit = suClub;
            back = cdFaceDown1;
        }
        //Non-default Constructor: Initializes card to specific rank and suit
        public Card(int ra, int su)
        {
            rank = ra;
            suit = su;
            back = cdFaceDown1;
        }
        public bool Draw(IntPtr hdc, int x, int y, modes md)
        {       
          if(md == modes.Up)
            return cdtDraw(hdc, x, y, Cd(rank, suit), (int)md, 0);
          return cdtDraw(hdc, x, y, Back, (int)md, 0);
        }
        public bool Initialize()
        {
            int dxcard ,dycard ;
            //unsafe
            //{
                return cdtInit(out dxcard, out dycard);
            //}     
        }
        public int Back
        {
            get 
            { 
                return back; 
            }
            set 
            { 
                back = value; 
            }
        }
        public void Terminate()
        {
            cdtTerm();
        }
        public int Suit
        {
            set
            {
                suit = value;
            }
            get
                {
                    return suit;
                }
        }
        public int Rank
        {
            set
            {
                rank = value;
            }
            get
            {
                return rank;
            }
        }
       
        //return card from rank and suit
        public int Cd(int ra, int su)
        {
            return (((ra) << 2) | (su));
        }
    }
    public class Deck
    {

    }
}
