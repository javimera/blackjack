﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Windows_Assignment4
{
    public partial class DeckNumber : Form
    {
        public string NumberOfDecks
        {
            get;
            private set;            
        }

        public DeckNumber(string number = "1")
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            this.NumberOfDecks = number;            
        }

        private void okButton_Click(object sender, EventArgs e)
        {            
            this.NumberOfDecks = this.numberOfDecksUpDownControl.Value.ToString();

            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeckNumber_Load(object sender, EventArgs e)
        {
            this.numberOfDecksUpDownControl.Value = Convert.ToDecimal(this.NumberOfDecks);
        }
    }
}
