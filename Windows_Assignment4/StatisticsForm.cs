﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Windows_Assignment4
{
    public partial class StatisticsForm : Form
    {
        private int wins;
        private int losses;

        public StatisticsForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            this.wins = 0;
            this.losses = 0;
        }
       
        private void StatisticsForm_Load(object sender, EventArgs e)
        {
            this.InitiazlieTextBoxes();
        }

        public void AddWin()
        {
            this.winsTextBox.Text = this.wins++.ToString();
        }

        public void AddLoss()
        {
            this.lossesTextBox.Text = this.losses++.ToString();
        }

        public void ResetScore()
        {
            this.winsTextBox.Clear();
            this.lossesTextBox.Clear();
            this.wins = 0;
            this.losses = 0;
            this.InitiazlieTextBoxes();
        }

        private void InitiazlieTextBoxes()
        {
            this.winsTextBox.Text = this.wins.ToString();
            this.lossesTextBox.Text = this.losses.ToString();
        }
    }
}
