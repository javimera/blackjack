﻿namespace Windows_Assignment4
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WinsLabel = new System.Windows.Forms.Label();
            this.lossesLabel = new System.Windows.Forms.Label();
            this.winsTextBox = new System.Windows.Forms.TextBox();
            this.lossesTextBox = new System.Windows.Forms.TextBox();
            this.OKButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // WinsLabel
            // 
            this.WinsLabel.AutoSize = true;
            this.WinsLabel.Location = new System.Drawing.Point(12, 24);
            this.WinsLabel.Name = "WinsLabel";
            this.WinsLabel.Size = new System.Drawing.Size(31, 13);
            this.WinsLabel.TabIndex = 0;
            this.WinsLabel.Text = "Wins";
            // 
            // lossesLabel
            // 
            this.lossesLabel.AutoSize = true;
            this.lossesLabel.Location = new System.Drawing.Point(12, 76);
            this.lossesLabel.Name = "lossesLabel";
            this.lossesLabel.Size = new System.Drawing.Size(40, 13);
            this.lossesLabel.TabIndex = 1;
            this.lossesLabel.Text = "Losses";
            // 
            // winsTextBox
            // 
            this.winsTextBox.Location = new System.Drawing.Point(94, 24);
            this.winsTextBox.Name = "winsTextBox";
            this.winsTextBox.ReadOnly = true;
            this.winsTextBox.Size = new System.Drawing.Size(100, 20);
            this.winsTextBox.TabIndex = 2;
            this.winsTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lossesTextBox
            // 
            this.lossesTextBox.Location = new System.Drawing.Point(94, 73);
            this.lossesTextBox.Name = "lossesTextBox";
            this.lossesTextBox.ReadOnly = true;
            this.lossesTextBox.Size = new System.Drawing.Size(100, 20);
            this.lossesTextBox.TabIndex = 3;
            this.lossesTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // OKButton
            // 
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.OKButton.Location = new System.Drawing.Point(119, 142);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 4;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.OKButton;
            this.ClientSize = new System.Drawing.Size(229, 193);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.lossesTextBox);
            this.Controls.Add(this.winsTextBox);
            this.Controls.Add(this.lossesLabel);
            this.Controls.Add(this.WinsLabel);
            this.MaximizeBox = false;
            this.Name = "StatisticsForm";
            this.Text = "Game Stats";
            this.Load += new System.EventHandler(this.StatisticsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label WinsLabel;
        private System.Windows.Forms.Label lossesLabel;
        private System.Windows.Forms.TextBox winsTextBox;
        private System.Windows.Forms.TextBox lossesTextBox;
        private System.Windows.Forms.Button OKButton;
    }
}