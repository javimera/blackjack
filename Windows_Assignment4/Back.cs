﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Windows_Assignment4
{
    public partial class Back : Form
    {
        public string Selected
        {
            get;
            private set;
        }

        public Back(string selected = "54")                        
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterParent;
            this.Selected = selected;
        }

        private void Back_Load(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    if (rb.Text.Equals(this.Selected))
                    {
                        rb.Checked = true;
                    }
                }
            }
        }          

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is RadioButton)
                {
                    RadioButton rb = (RadioButton)c;
                    
                    if (rb.Checked)
                    {
                        this.Selected = rb.Text;
                    }
                }
            }

            this.Close();
        }       
    }
}
