﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using CardsClass;

namespace Windows_Assignment4
{
    class Deck
    {
        private Card[] cards;
        
        public Deck()
        {
            this.cards = new Card[52];            
            this.InitializeDeck();
        }

        private void InitializeDeck()
        {
            int cardCounter = 0;

            for (int suit = 0; suit < 4; suit++)
            {
                for (int rank = 0; rank < 13; rank++)
                {
                    this.cards[cardCounter++] = new Card(rank, suit);
                } 
            }            
        }

        public Card getCard(int position)
        {
            return this.cards[position];
        }

        public void setCard(int rank, Card card)
        {
            this.cards[rank] = card;
        }

        public int getDeckLength()
        {
            return this.cards.Length;
        }

        public void ChangeBackCard(int back)
        {
            for (int c = 0; c < this.cards.Length; c++)
            {
                this.cards[c].Back = back;
            }
        }
    }
}
