﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CardsClass;

namespace Windows_Assignment4
{
    public partial class BlackJack : Form
    {
        private bool IsDealerPlaying; // indicates if the dealer is playing or not
        private bool IsPlaying; // indicates if player is playing or not
        private bool IsBlackJack; // indicates is the first deal is a blackjack
        private bool IsShoeEmpty; // indicates if the shoe is empty
        private bool IsHandFinished; // indicates if the current hand is done or not
        private int currentCard; // current card in the shoe
        private int currentDeck; // current deck being used in the shoe        
        private int shoeSize; // size of the shoe
        private int backSelected; // keeps track of the back card selected

        // BlackJack has a Player
        private Player player;

        // BlackJack has a Dealer
        private Dealer dealer;

        // Shoe of the game
        private Shoe myShoe;

        // statistics object
        private StatisticsForm stats;

        // Points to draw cards accordingly to either the player, or dealer.
        private Point dealerLocation;
        private Point playerLocation;

        private Card cardTopLeft;

        public BlackJack()
        {
            this.InitializeComponent();
            this.InitializeButtons();
            this.StartPosition = FormStartPosition.CenterScreen;
            
            // By default the shoe will contain only 1 deck
            this.shoeSize = 1;

            // create a any card to be drawn on the top left corner of the table that will either display a circle, x or current card in the shoe
            this.cardTopLeft = new Card(0,0);            
        }

        private void BlackJack_Load(object sender, EventArgs e)
        {
            // Get the dealer's position in table to draw the cards accordingly
            this.dealerLocation = new Point(this.dealerLabel.Location.X, this.dealerLabel.Location.Y);

            // Get the player's position in table to draw the cards accordingly
            this.playerLocation = new Point(this.playerLabel.Location.X, this.playerLabel.Location.Y);    
        
            // disable the shuffle option
            this.shuffleToolStripMenuItem.Enabled = false;

            this.IsHandFinished = false;

            this.backSelected = 54; // default value
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void OnPaint(PaintEventArgs e)
        {                        
            // draw X card if the hand has ended
            if (this.IsShoeEmpty)
            {
                this.cardTopLeft.Draw(5, 30, Card.modes.DeckX, e);
            }
            else
            {                
                // Draw card facing down from deck, on the top left corner of the table
                if (this.myShoe == null)
                {
                    this.cardTopLeft.Draw(5, 30, Card.modes.DeckO, e);
                }
                else if (this.myShoe != null)
                {
                    this.cardTopLeft.Draw(5, 30, Card.Face.Down, e);
                }
            }
    
            if(this.player != null)
                this.player.DrawCards(this.playerLabel.Location.X, this.playerLabel.Location.Y, e);

            if(this.dealer != null)
                this.dealer.DrawCards(this.dealerLabel.Location.X, this.dealerLabel.Location.Y, this.IsHandFinished, e);
            
            base.OnPaint(e);
        }

        private void newHandButton_Click(object sender, EventArgs e)
        {
            this.NewHand();

            this.playerAmountLabel.Text = "Amount: ";
            this.dealerAmountLabel.Text = "Amount: ";

            if (!(this.myShoe == null))
            {
                if (this.DealTwoToPlayerAndDealer())
                {                    
                    // check for black jack
                    if (this.player.FirstDeal())
                    {
                        this.IsBlackJack = true;
                        this.playerAmountLabel.Text = "Amount: " + this.player.Score.ToString();
                        this.stats.AddWin();                        
                        MessageBox.Show("Player blackjack");
                    }
                    else
                    {
                        // count the cards normally, without looking for a black jack
                        this.player.CalculateScore();
                        this.playerAmountLabel.Text += "  " + this.player.Score.ToString();
                    }

                    // check for black jack if the player didnt get a black jack
                    if (!this.IsBlackJack)
                    {
                        if (this.dealer.FirstDeal())
                        {
                            this.IsBlackJack = true;
                            this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score.ToString();
                            this.stats.AddLoss();
                            MessageBox.Show("Dealer blackJack");
                        }
                        else
                        {
                            // count the cards normally, without looking for a black jack
                            this.dealer.CalculateScore();
                        }
                    }
                    // check for a black jack on the first deal
                    if (!this.IsBlackJack)
                    {
                        // if there is no blackjack, enable the hit and stay buttons to allow the player to continue playing  
                        this.AllowPlayerPlayAfterFirstDeal();                                              
                    }
                    else
                    {
                        // allow options to change the number of decks, and/or the back of the card because the hand finished due to a blackjack
                        this.HandFinished();
                    }

                    // check if the player hits the last card of the shoe
                    if (this.currentDeck == this.shoeSize - 1 && this.currentCard == 52)
                    {                                           
                        this.DisablePlayerButtons();
                        this.ContinuePlayingAfterNewShoe();                        
                    }
                    this.Invalidate();
                }                                
                else
                {
                    this.ReShuffleWhilePlaying();                    
                }
            }
            else
            {
                MessageBox.Show("There is no shoe. Start a new game");
            }
        }
        
        private void hitButton_Click(object sender, EventArgs e)
        {            
            if (this.myShoe.IsShuffled)
            {
                // check if the end of the current deck has been reached, in order to reset the card counter when the player is hitting
                if (this.currentCard == 52)
                {
                    if (this.currentDeck != this.shoeSize - 1)
                    {
                        this.currentCard = 0;
                        this.currentDeck++;
                    }
                    else
                    {
                        MessageBox.Show("No more cards", "Reshuffle the shoe");
                        this.DisablePlayerButtons();
                        this.ContinuePlayingAfterNewShoe();
                        
                        this.Invalidate();
                    }
                }
                
                if(this.currentCard < 52)
                {
                    // call invalidate before checking the end of the deck so the last card is displayed approprietaly            
                    this.Invalidate();

                    Card c = this.myShoe.Deal(this.currentDeck, this.currentCard);
                    this.player.AddCard(c);

                    this.currentCard++;

                    if (!this.player.CalculateScore())
                    {
                        MessageBox.Show("Player loses");
                        this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score.ToString();
                        this.stats.AddLoss();
                        this.WinLose();
                    }
                    else
                    {
                        if (this.player.GetCards().Count == 5)
                        {
                            MessageBox.Show("WIN!!");
                            this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score.ToString();
                            this.stats.AddWin();
                            this.WinLose();
                        }
                    }

                    this.playerAmountLabel.Text = "Amount: " + this.player.Score.ToString();
                }
            }
            else
            {
                MessageBox.Show("The shoe hasn't been shuffled yet", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ContinuePlayingAfterNewShoe()
        {               
            this.backToolStripMenuItem.Enabled = true;
            this.numberOfDecksToolStripMenuItem.Enabled = true;
            this.shuffleToolStripMenuItem.Enabled = true;
            this.IsPlaying = true;
            this.IsShoeEmpty = true;
        }

        private void stayButton_Click(object sender, EventArgs e)
        {            
            this.StartDealer();            
        }

        private void InitializeShoe()
        {            
            this.myShoe = new Shoe(this.shoeSize);
            this.myShoe.SetBackCard(this.backSelected);            
            this.InitializeShoeCounters();
            this.InitializeButtons();
            this.IsShoeEmpty = false;            
        }

        private void InitializeButtons()
        {
            this.hitButton.Enabled = false;
            this.newHandButton.Enabled = true;
            this.stayButton.Enabled = false;
        }

        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Back backForm = new Back(this.cardTopLeft.Back.ToString());
            DialogResult result = backForm.ShowDialog();

            if (result == DialogResult.OK)
            {
                this.cardTopLeft.Back = Convert.ToInt32(backForm.Selected);

                // set the back of the card of the entire shoe
                this.backSelected = Convert.ToInt32(backForm.Selected);

                // check if the shoe already exists
                // If it doesn't exist, the new backcard will be shown when the shoe is created
                if (this.myShoe != null)
                {
                    this.myShoe.SetBackCard(this.backSelected);
                }

                // enable this option only if there is a current game going on
                if(this.myShoe != null && !this.shuffleToolStripMenuItem.Enabled)
                    this.shuffleToolStripMenuItem.Enabled = true;

                // disable player's buttons until the new shoe is reshuffled
                this.DisablePlayerButtons();
                this.Invalidate();
            }            
        }
        
        private void shuffleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.myShoe == null)
            {
                MessageBox.Show("There is no shoe to be shuffled. Start a new game.");
            }
            else
            {
                this.InitializeShoe();
                this.shuffleToolStripMenuItem.Enabled = false;
                this.hitButton.Enabled = true;
                this.stayButton.Enabled = true;

                // checks if the dealer is playing and ran out of cards
                if (this.IsDealerPlaying)
                {
                    this.hitButton.Enabled = false;
                    this.stayButton.Enabled = false;
                    this.IsDealerPlaying = false;
                    this.StartDealer();
                }
                else
                {
                    this.hitButton.Enabled = true;
                    this.stayButton.Enabled = true;
                }

                // checks for the player and ran out of cards
                if (!this.IsPlaying)
                {
                    this.player = new Player();
                    this.dealer = new Dealer();
                    this.newHandButton.Enabled = true;
                    this.hitButton.Enabled = false;
                    this.stayButton.Enabled = false;
                }
                
                this.Invalidate();
            }
        }

        private void numberOfDecksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeckNumber deckNumber = new DeckNumber(this.shoeSize.ToString());
            DialogResult result = deckNumber.ShowDialog();

            // check if the player clicked OK, or Cancel when changing the number of decks
            if (result == DialogResult.OK)
            {
                // Store the amount of decks selected from the menu option
                this.shoeSize = Convert.ToInt32(deckNumber.NumberOfDecks);

                // disable player's buttons until the new shoe has been created
                this.DisablePlayerButtons();

                // enable shuffle only if it's disabled, don't enable it twice
                if (this.myShoe != null && !this.shuffleToolStripMenuItem.Enabled)
                    this.shuffleToolStripMenuItem.Enabled = true;
            }
        }

        private bool checkEndOfShoe()
        {
            // check if the deck counter is the last deck in the shoe
            if(this.currentDeck == this.myShoe.getShoeLength())
            {
                // indicate that the shoe is empty
                this.IsShoeEmpty = true;                

                // return true indicating that the current hand has ended
                return true;
            }

            // return false if the shoe is not empty yet
            return false;
        }

        private bool checkCardCount()
        {
            if(this.currentCard == 52)
            {
                // check if there are any available decks
                if (!this.checkEndOfShoe())
                {
                    // if there are more decks available, reset the card counter
                    MessageBox.Show("Playing next deck");                    
                    this.Invalidate();
                }
                else
                {
                    // return true if the end of the shoe has been reached
                    this.EndOfShoe();

                    return true;
                }
            }

            // return false if neither the end of the shoe has been reached, nor if the current card is less than 52
            return false;
        }

        private void StartDealer()
        {
            this.hitButton.Enabled = false;
            this.stayButton.Enabled = false;            

            // check if there are no cards left in the current deck                       
            while(this.currentCard <= 52)
            {
                if (this.currentCard == 52)
                {
                    if (this.currentDeck != this.shoeSize - 1)
                    {
                        this.currentCard = 0;
                        this.currentDeck++;
                    }
                    else
                    {
                        MessageBox.Show("No more cards");
                        this.DisablePlayerButtons();
                        this.ContinuePlayingAfterNewShoe();
                        this.IsDealerPlaying = true;
                        break;
                    }
                }

                if (!this.dealer.CalculateScore())
                {
                    if (this.dealer.Score > 21)
                    {                        
                        this.ResetHandVariables();
                        this.stats.AddWin();
                        this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score;
                        MessageBox.Show("Dealer busts!. PlayerWins");
                    }
                    else if (this.player.Score > this.dealer.Score)
                    {                        
                        this.ResetHandVariables();
                        this.stats.AddWin();
                        this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score;
                        MessageBox.Show("Player Wins");
                    }
                    else if (this.player.Score < this.dealer.Score)
                    {                        
                        this.ResetHandVariables();
                        this.stats.AddLoss();
                        this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score;
                        MessageBox.Show("Dealer Wins");
                    }
                    else
                    {                     
                        this.ResetHandVariables();
                        this.dealerAmountLabel.Text = "Amount: " + this.dealer.Score;
                        MessageBox.Show("It's a Push!", "Nobody Wins or loses");
                    }

                    this.IsHandFinished = true;

                    // enable back and number of decks option
                    this.backToolStripMenuItem.Enabled = true;
                    this.numberOfDecksToolStripMenuItem.Enabled = true;

                    break;
                }
                else
                {                    
                    Card c = this.myShoe.Deal(this.currentDeck, this.currentCard);
                    this.dealer.AddCard(c);

                    // if the dealer can keep hitting, increment the current card to get the next card from the shoe
                    this.currentCard++;
                }                
            }
            
            // call invalidate before checking the end of the deck so the last card is displayed approprietaly                        
            this.Invalidate();
        }       

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            // initialize the shoe when a new hand begins
            this.InitializeShoe();

            this.InitializeButtons();

            // create player and dealer for the new game
            this.player = new Player();
            this.dealer = new Dealer();

            // set this back to false for a new game
            this.IsPlaying = false;

            // statistics object instanciation
            this.stats = new StatisticsForm();

            this.Invalidate();
        }
               
        private void statisticsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.stats != null)
            {
                this.stats.ShowDialog();
            }
            else
            {
                MessageBox.Show("No Games have been created yet", "Create a new Game", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rulesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Rules rules = new Rules();
            rules.ShowDialog();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.ShowDialog();
        }

        private void WinLose()
        {           
            this.ResetHandVariables();
            this.backToolStripMenuItem.Enabled = true;
            this.numberOfDecksToolStripMenuItem.Enabled = true;
            this.IsHandFinished = true;
            this.Invalidate();
        }

        private void ReShuffleWhilePlaying()
        {
            MessageBox.Show("Shoe is running out of cards.", "Reshuffle");
            this.backToolStripMenuItem.Enabled = true;
            this.numberOfDecksToolStripMenuItem.Enabled = true;
            this.DisablePlayerButtons();
            this.shuffleToolStripMenuItem.Enabled = true;
        }

        private void HandFinished()
        {
            this.backToolStripMenuItem.Enabled = true;
            this.numberOfDecksToolStripMenuItem.Enabled = true;
            this.IsHandFinished = true;
        }

        private void AllowPlayerPlayAfterFirstDeal()
        {
            this.stayButton.Enabled = true;
            this.hitButton.Enabled = true;
            this.newHandButton.Enabled = false;
        }

        private void NewHand()
        {
            this.IsBlackJack = false;
            this.IsHandFinished = false;
            this.backToolStripMenuItem.Enabled = false;
            this.numberOfDecksToolStripMenuItem.Enabled = false;
            this.IsDealerPlaying = false;
        }
        private void InitializeShoeCounters()
        {
            this.currentCard = 0;
            this.currentDeck = 0;
        }

        private void ResetHandVariables()
        {
            this.newHandButton.Enabled = true;
            this.IsHandFinished = true;
            this.hitButton.Enabled = false;
            this.stayButton.Enabled = false;
            this.IsBlackJack = false;
        }

        private void DisablePlayerButtons()
        {
            this.stayButton.Enabled = false;
            this.hitButton.Enabled = false;
            this.newHandButton.Enabled = false;
        }

        private void EndOfShoe()
        {
            this.backToolStripMenuItem.Enabled = true;
            this.numberOfDecksToolStripMenuItem.Enabled = true;
            this.IsShoeEmpty = true;
            this.Invalidate();
        }

        // Returns false if a new hand can't be started
        // Returns true if a new can start
        private bool DealTwoToPlayerAndDealer()
        {
            // create player and dealer object for every new hand
            this.player = new Player();
            this.dealer = new Dealer();

            if (!this.IsShoeEmpty)
            {
                // check if it's the last deck & if the last 3, 2 or 1 card(s) of the deck are remaining                    
                if (this.currentCard + 3 > 51 && this.currentDeck == this.shoeSize - 1)
                {
                    // if so, return false and reshuffle. Dont play those last remaining cards
                    return false;
                }
                else
                {
                    List<Card> firstDeal = new List<Card>();
                    int offset = 4;

                    for (int i = 0; i < offset; i++)
                    {
                        if (this.currentCard == 52)
                        {
                            // if there are more decks, but its not the last deck, then reset the card counter
                            if (this.currentDeck != this.shoeSize - 1)
                            {
                                this.currentCard = 0;
                                offset -= i;
                                i = 0;
                            }
                            else
                            {
                                return false;
                            }

                            this.currentDeck++;
                        }

                        if (this.currentCard < 52)
                        {
                            firstDeal.Add(this.myShoe.Deal(this.currentDeck, this.currentCard));
                            this.currentCard++;
                        }
                    }

                    this.player.AddCard(firstDeal[0]);
                    this.dealer.AddCard(firstDeal[1]);
                    this.player.AddCard(firstDeal[2]);
                    this.dealer.AddCard(firstDeal[3]);

                    return true;
                }
            }
            else
            {
                MessageBox.Show("Empty Shoe");
            }

            return false;
        }        
    }
}