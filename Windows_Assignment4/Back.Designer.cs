﻿namespace Windows_Assignment4
{
    partial class Back
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.okButton = new System.Windows.Forms.Button();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(228, 403);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(97, 45);
            this.okButton.TabIndex = 13;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton7.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton7.Image = global::Windows_Assignment4.Properties.Resources._65;
            this.radioButton7.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton7.Location = new System.Drawing.Point(558, 211);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(71, 126);
            this.radioButton7.TabIndex = 11;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "65";
            this.radioButton7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton8.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton8.Image = global::Windows_Assignment4.Properties.Resources._64;
            this.radioButton8.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton8.Location = new System.Drawing.Point(449, 211);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(71, 126);
            this.radioButton8.TabIndex = 10;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "64";
            this.radioButton8.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton9.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton9.Image = global::Windows_Assignment4.Properties.Resources._63;
            this.radioButton9.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton9.Location = new System.Drawing.Point(342, 211);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(71, 126);
            this.radioButton9.TabIndex = 9;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "63";
            this.radioButton9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton10.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton10.Image = global::Windows_Assignment4.Properties.Resources._62;
            this.radioButton10.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton10.Location = new System.Drawing.Point(228, 211);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(71, 126);
            this.radioButton10.TabIndex = 8;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "62";
            this.radioButton10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton11.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton11.Image = global::Windows_Assignment4.Properties.Resources._61;
            this.radioButton11.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton11.Location = new System.Drawing.Point(119, 211);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(71, 126);
            this.radioButton11.TabIndex = 7;
            this.radioButton11.TabStop = true;
            this.radioButton11.Text = "61";
            this.radioButton11.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton12.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton12.Image = global::Windows_Assignment4.Properties.Resources._60;
            this.radioButton12.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton12.Location = new System.Drawing.Point(12, 211);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(71, 126);
            this.radioButton12.TabIndex = 6;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "60";
            this.radioButton12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton4.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton4.Image = global::Windows_Assignment4.Properties.Resources._59;
            this.radioButton4.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton4.Location = new System.Drawing.Point(558, 36);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(71, 126);
            this.radioButton4.TabIndex = 5;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "59";
            this.radioButton4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton5.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton5.Image = global::Windows_Assignment4.Properties.Resources._58;
            this.radioButton5.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton5.Location = new System.Drawing.Point(449, 36);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(71, 126);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "58";
            this.radioButton5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton6.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton6.Image = global::Windows_Assignment4.Properties.Resources._57;
            this.radioButton6.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton6.Location = new System.Drawing.Point(342, 36);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(71, 126);
            this.radioButton6.TabIndex = 3;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "57";
            this.radioButton6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton3.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton3.Image = global::Windows_Assignment4.Properties.Resources._56;
            this.radioButton3.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton3.Location = new System.Drawing.Point(228, 36);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(71, 126);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "56";
            this.radioButton3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton2.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton2.Image = global::Windows_Assignment4.Properties.Resources._55;
            this.radioButton2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton2.Location = new System.Drawing.Point(119, 36);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(71, 126);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "55";
            this.radioButton2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.CheckAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton1.ForeColor = System.Drawing.SystemColors.Control;
            this.radioButton1.Image = global::Windows_Assignment4.Properties.Resources._54;
            this.radioButton1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.radioButton1.Location = new System.Drawing.Point(12, 36);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(71, 126);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "54";
            this.radioButton1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.radioButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(342, 403);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(97, 45);
            this.cancelButton.TabIndex = 14;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Back
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(698, 476);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.radioButton7);
            this.Controls.Add(this.radioButton8);
            this.Controls.Add(this.radioButton9);
            this.Controls.Add(this.radioButton10);
            this.Controls.Add(this.radioButton11);
            this.Controls.Add(this.radioButton12);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton5);
            this.Controls.Add(this.radioButton6);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Name = "Back";
            this.Text = "Select a Back Card";
            this.Load += new System.EventHandler(this.Back_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;        
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;

    }
}