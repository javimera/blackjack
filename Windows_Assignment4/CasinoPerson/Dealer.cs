﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CardsClass;

namespace Windows_Assignment4
{
    public class Dealer : BlackJackPerson
    {
        public Dealer()
            : base()
        { 
        }

        public void DrawCards(int x, int y, bool handFinished, PaintEventArgs e)
        {
            int offset = 0;

            foreach (Card c in this.listOfCards)
            {
                if (handFinished)
                {
                    c.Draw(x + 50 + offset, y, Card.Face.Up, e);
                }
                else
                {
                    c.Draw(x + 50 + offset, y, offset == 0 ? Card.Face.Down : Card.Face.Up, e);
                }
                offset += 20;
            }
        }

        public bool CalculateScore()
        {
            this.Score = 0;

            this.AddNoAceCards();

            if (!this.AddAceCards())
                return false;

            if (this.Score > 16)
            {
                return false;
            }

            // if both lists of no ace cards and ace cards were successfully added, then return true meaning that the player can keep hitting
            return true;
        }
    }
}
