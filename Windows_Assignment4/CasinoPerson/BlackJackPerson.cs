﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using CardsClass;

namespace Windows_Assignment4
{
    public class BlackJackPerson
    {
        protected List<Card> listOfCards;
        public int Score { get; protected set; }

        public BlackJackPerson()
        {
            this.listOfCards = new List<Card>();
            this.Score = 0;
        }

        public void AddCard(Card card)
        {
            this.listOfCards.Add(card);
        }

        public List<Card> GetCards()
        {
            return this.listOfCards;
        }

        public bool FirstDeal()
        {
            if (this.CheckBlackJack(this.listOfCards[0].Rank, this.listOfCards[1].Rank))
            {
                return true;
            }

            return false;
        }

        private bool CheckBlackJack(int card1, int card2)
        {
            // check if the first card is an ace, then check the next card
            if (card1 == 0)
            {
                if (card2 > 8 && card2 < 13)
                {
                    this.Score = 21;
                    return true;
                }
            }

            // check if the second card is an ace, then check the previous card
            if (card2 == 0)
            {
                if (card1 > 8 && card1 < 13)
                {
                    this.Score = 21;
                    return true;
                }
            }

            // if there is no black jack, just add the points 
            this.Score += card1 > 9 && card1 < 13 ? 10 : card1 + 1;
            this.Score += card2 > 9 && card2 < 13 ? 10 : card2 + 1;

            return false;
        }

        protected void AddNoAceCards()
        {
            var noAceCards = from c in this.listOfCards
                             where c.Rank != 0
                             select c;

            foreach (Card c in noAceCards)
            {
                if (c.Rank > 9 && c.Rank < 13)
                {
                    this.Score += 10;
                }

                else
                {
                    this.Score += c.Rank + 1;
                }
            }
        }

        protected bool AddAceCards()
        {
            var AceCards = from c in this.listOfCards
                           where c.Rank == 0
                           select c;

            foreach (Card c in AceCards)
            {
                // if the amount of aces is greater than 1, and the score already exceeds 10, then count each ace as 1
                if (this.Score >= 10 && AceCards.Count() > 1)
                {
                    this.Score += 1;
                }
                // else check if the ace should be counted as 11 or 1
                else
                {
                    if (this.Score + 11 < 22)
                    {
                        this.Score += 11;
                    }
                    else if (this.Score + 1 < 22)
                    {
                        this.Score += 1;
                    }
                    else
                    {
                        this.Score += 1;
                        return false;
                    }
                }
            }

            return true;
        }
    }
}
