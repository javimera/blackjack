﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardsClass;

namespace Windows_Assignment4
{
    class Shoe
    {
        private List<Deck> shoeList;
        private int numDecks;

        public bool IsShuffled
        {
            get;
            private set;
        }

        public Shoe(int numDecks)
        {
            this.numDecks = numDecks;
            this.IsShuffled = false;
            this.InitializeShoe();
            this.Shuffle(); 
        }

        private void InitializeShoe()
        {
            this.shoeList = new List<Deck>();

            for (int suit = 0; suit < this.numDecks; suit++)
            {
                Deck newDeck = new Deck();
                this.shoeList.Add(newDeck);
            }
        }

        public int getShoeLength()
        {
            return this.numDecks;
        }

        public Deck getDeck(int position)
        {
            return this.shoeList[position];
        }

        public void Shuffle()
        {            
            Random rand = new Random();
            int randDeck;
            int randRank;

            for (int deck = 0; deck < this.numDecks; deck++)
            {
                int howManyCards = this.shoeList[deck].getDeckLength();

                for (int rank = 0; rank < howManyCards; rank++)
                {
                    randDeck = rand.Next(0, this.numDecks);
                    randRank = rand.Next(0, 52);

                    Card card1 = shoeList[deck].getCard(rank);
                    Card card2 = shoeList[randDeck].getCard(randRank);

                    swap(ref card1, ref card2);

                    shoeList[deck].setCard(rank, card1);
                    shoeList[randDeck].setCard(randRank, card2);
                }
            }

            this.IsShuffled = true;
        }

        private void swap(ref Card card1, ref Card card2)
        {
            CardsClass.Card card = card1;
            card1 = card2;
            card2 = card;
        }

        public void SetBackCard(int back)
        {
            for (int deck = 0; deck < this.numDecks; deck++)
            {
                this.shoeList[deck].ChangeBackCard(back);
            }
        }

        public Card Deal(int currentDeck, int currentCard)
        {
            return this.shoeList[currentDeck].getCard(currentCard);
        }
    }
}
